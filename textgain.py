#!/usr/bin/env python
# -*- coding: utf-8 -*-

# >>> import textgain
# >>> textgain.key = "<YOUR PERSONAL KEY>"
# >>> q = "A nice and simple Python API."
# >>> print(textgain.sentiment(q, language="en"))
# >>> print(textgain.profile(q, language="en", by="Tom"))

# sentiment() returns a (polarity, confidence) tuple.
# profile() returns a dict of results from sentiment(), age(), gender(), ...

# (1.0, 0.7)
# {  'language': 'en', 
#    'concepts': ['api', 'python'], 
#   'sentiment': 1.0, 
#         'age': '25+', 
#      'gender': 'm', 
#   'education': '+', 
# 'personality': ''}

#------------------------------------------------------------------------------

__author__     =  "Textgain"
__authors__    = ["Tom De Smedt", "Guy De Pauw"]
__contact__    =  "info@textgain.com"
__copyright__  =  "Copyright 2016, Textgain"
__version__    =  "1.0"

import sys
import json

if sys.version.startswith("3"):
    from urllib.request import Request
    from urllib.request import urlopen
    from urllib.parse   import urlencode

if sys.version.startswith("2"):
    from urllib2 import Request
    from urllib2 import urlopen
    from urllib  import urlencode

#------------------------------------------------------------------------------

key = "" # API key

#------------------------------------------------------------------------------

API = "https://api.textgain.com/1/"

def str(s, encoding="utf-8"):
    """ Returns the given string as a Unicode string.
    """
    try:
        return s.decode(encoding)
    except:
        return s

def bytes(s, encoding="utf-8"):
    """ Returns the given string as a bytestring.
    """
    try:
        return s.encode(encoding)
    except:
        return s

def request(url, **q):
    """ Returns the content of the given URL,
        where q is a dictionary of POST data.
    """
    q = dict((bytes(k), bytes(v)) for k, v in q.items())
    q = urlencode(q)
    r = urlopen(Request(url, bytes(q)))
    r = r.read()
    r = str(r)
    return r

class LimitExceeded(Exception):
    pass

def api(node, s, **q):
    """ Returns the response of the given Textgain node
        (sentiment, age, gender, ...), as a dictionary.
    """
    # api("age", "hi", language="en") => {"age": "25-", "confidence": 0.75}
    q = {
           "q": s,
        "lang": q.pop("language", q.pop("lang", "en")),
         "key": q.pop("key", key),
         "top": q.pop("top", 0  ),
          "by": q.pop("by" , "" ),
    }
    try:
        r = request(API + node, **q)
        r = json.loads(r)
    except Exception as e:
        if getattr(e, "code", 0) == 429:
            e = LimitExceeded()
            e.__cause__ = None
            raise e
        else:
            raise e
    return r

def values(dict, *keys):
    """ Returns a tupple of values for the given dictionary keys.
    """
    return tuple(dict[k] for k in keys if k in dict)

#------------------------------------------------------------------------------

F = "confidence" # (F-score)

def language(s, **k):
    n = "language"
    return values(api(n, s, **k), n, F)

def genre(s, **k):
    n = "genre"
    return values(api(n, s, **k), n, F)

def tag(s, **k):
    n = "tag"
    return values(api(n, s, **k), "text", F)

def concepts(s, **k):
    n = "concepts"
    return values(api(n, s, **k), n)

def sentiment(s, **k):
    n = "sentiment"
    return values(api(n, s, **k), "polarity", F)

def age(s, **k):
    n = "age"
    return values(api(n, s, **k), n, F)

def gender(s, **k):
    n = "gender"
    return values(api(n, s, **k), n, F)

def education(s, **k):
    n = "education"
    return values(api(n, s, **k), n, F)

def personality(s, **k):
    n = "personality"
    return values(api(n, s, **k), n, F)

_language, tags = language, tag

#------------------------------------------------------------------------------

def profile(s, language=None, by=""):
    """ Returns a dictionary with the predicted profile of the given string
        (language, concepts, sentiment, age, gender, education, ...)
    """
    if language:
        x = language
    else:
        x =_language(s)[0] # guess language
    return {
        u"language"    : str(x),
        u"concepts"    : (concepts(s, lang=x, top=3) + ("",))[0],
        u"sentiment"   : (sentiment(s, lang=x)       + ("",))[0],
        u"age"         : (age(s, lang=x)             + ("",))[0],
        u"gender"      : (gender(s, lang=x, by=by)   + ("",))[0],
        u"education"   : (education(s)               + ("",))[0],
        u"personality" : (personality(s, lang=x)     + ("",))[0]
    }
